// Global domain
const host = 'http://localhost:5282/api/ProductApi';
// Ensure DOM content is fully loaded before attaching event listeners
document.addEventListener('DOMContentLoaded', function () {
	// Fetch and display products on page load
	fetchProducts();
	// Add event listener for adding a product
	document.getElementById('btnAdd').addEventListener('click', addProduct);
	// Add event listener for updating a product
	document.getElementById('btnUpdate').addEventListener('click', updateProduct);
});

let selectedProductId = null;

// Fetch products from the server and display them
function fetchProducts() {
	const apiUrl = `${host}`;
	fetch(apiUrl)
		.then(handleResponse)
		.then(data => displayProducts(data))
		.catch(error => console.error('Fetch error:', error.message));
}

// Handle fetch response, check for error, and parse JSON
function handleResponse(response) {
	if (!response.ok) throw new Error('Network response was not ok');
	return response.json();
}

// Display products in the HTML table
function displayProducts(products) {
	const bookList = document.getElementById('bookList');
	bookList.innerHTML = ''; // Clear existing products
	products.forEach(product => {
		bookList.innerHTML += createProductRow(product);
	});
	// After the products have been added to the DOM, attach event listeners
	attachEventListeners();
}

// Create HTML table row for a product
function createProductRow(product) {
	return `
        <tr>
            <td>${product.id}</td>
            <td>${product.name}</td>
            <td>${product.price}</td>
            <td>${product.description}</td>
            <td>
                <button class="btn btn-danger delete-btn" data-id="${product.id}">Delete</button>
								<button class="btn btn-warning edit-btn" data-id="${product.id}">Edit</button>
								<button class="btn btn-primary view-btn" data-id="${product.id}">View</button>
								</td>
        </tr>
    `;
}

function attachEventListeners() {
	// Attach edit event listeners
	document.querySelectorAll('.edit-btn').forEach(button => {
		button.addEventListener('click', editProduct);
	});
	// Attach delete event listeners
	document.querySelectorAll('.delete-btn').forEach(button => {
		button.addEventListener('click', deleteProduct);
	});
	// Attach delete event listeners
	document.querySelectorAll('.view-btn').forEach(button => {
		button.addEventListener('click', editProduct);
	});
}

// Add a new product
function addProduct() {
	const productData = {
		name: document.getElementById('bookName').value,
		price: document.getElementById('price').value,
		description: document.getElementById('description').value,
	};

	fetch(`${host}`, {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(productData),
	})
		.then(handleResponse)
		.then(data => {
			console.log('Product added:', data);
			fetchProducts(); // Refresh the product list
		})
		.catch(error => console.error('Error:', error));
}

// Edit product
function editProduct(event) {
	const productId = event.target.getAttribute('data-id');

	// Set the selected product ID for the update
	selectedProductId = productId;

	// Assuming you have a function to fetch a single product by ID
	fetch(`${host}`/`${productId}`)
		.then(handleResponse)
		.then(product => {
			// Populate the form fields with the product data
			document.getElementById('bookName').value = product.name;
			document.getElementById('price').value = product.price;
			document.getElementById('description').value = product.description;

			// Optionally, scroll to the form or highlight it
			document.getElementById('bookName').focus();
		})
		.catch(error => console.error('Error fetching product details:', error));

	// Update a product
	function updateProduct(event) {
		event.preventDefault();
		if (!selectedProductId) {
			alert('Please select a product to update.');
			return;
		}

		const updatedProduct = {
			id: +selectedProductId,
			name: document.getElementById('bookName').value,
			price: document.getElementById('price').value,
			description: document.getElementById('description').value,
		};

		fetch(`${host}` / `${selectedProductId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(updatedProduct),
		})
			.then(response => {
				if (response.ok) {
					if (response.status === 204) {
						console.log('Product updated successfully');
						// Reload the page to reflect the changes
						location.reload();
					} else {
						// If there is a JSON response, parse it
						return response.json();
					}
				} else {
					throw new Error('Failed to update product');
				}
			})
			.then(data => {
				// Handle the JSON data if any
				console.log(data);
			})
			.catch(error => console.error('Error:', error));
	}

	// Delete a product
	function deleteProduct(event) {
		const productId = event.target.getAttribute('data-id');
		fetch(`${host}` / `${productId}`, {
			method: 'DELETE',
		})
			.then(response => {
				if (response.ok) {
					console.log('Product deleted successfully.');
					fetchProducts(); // Refresh the product list
				} else {
					console.error('Failed to delete product.');
				}
			})
			.catch(error => console.error('Error:', error));
	}
}
